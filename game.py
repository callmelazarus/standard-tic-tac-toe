def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()


def game_over(board,parameter_2):
    print_board(board)
    print("GAME OVER")
    print(board[parameter_2], "has won")
    exit()

def is_row_winner(board,value):
    if value == 1:
        if board[0] == board[1] and board[1] == board[2]:  #is there a pattern that I can find to simplify this?
            return True 
    if value == 2:
        if board[3] == board[4] and board[4] == board[5]:  #there has to be 
            return True
    if value == 3:
        if board[6] == board[7] and board[7] == board[8]:  #hmmm
            return True
    
def is_column_winner(board,value):
    if value == 4:
        if board[0] == board[3] and board[3] == board[6]:
            return True
    if value == 5:
        if board[1] == board[4] and board[4] == board[7]:
            return True
    if value == 6: 
        if board[2] == board[5] and board[5] == board[8]:
            return True
            
def is_diag_winner(board,value):
    if value == 7:
        if board[0] == board[4] and board[4] == board[8]:
            return True
    if value == 8:
        if board[2] == board[4] and board[4] == board[6]:
            return True


board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board,1):
        game_over()
    elif is_row_winner(board,2):
        game_over(board,3) # work on logic to automate second argument
    elif is_row_winner(board,3):
        game_over(board,6)
    elif is_column_winner(board,4):
        game_over(board,0)
    elif is_column_winner(board,5):
        game_over(board,1)
    elif is_column_winner(board,6):
        game_over(board,2)
    elif is_diag_winner(board,7):
        game_over(board,0)
    elif is_diag_winner(board,8):
        game_over(board,0)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
